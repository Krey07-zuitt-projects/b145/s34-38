const Course = require('../models/Course');
const User = require('../models/User');


// Add a course

module.exports.addCourse = (reqBody,isAdmin) => {

if(isAdmin){

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course,err) => {

		if (err) {
			return false
		} else{
			return course;
		}
	})
}
	return Promise.resolve('Login Declined. You need to be the admin to access this page')
} 

// Retrievig all courses

module.exports.getAllCourses = async (user) => {
	if(user.isAdmin === true){
		return Course.find({}).then(result => {
			return result
		})
	} else {
		return `${user.email} is not authorized`
	}
}

// Retrieval of active courses

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

// Retrieve of specific course

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}

// Update a course

module.exports.updateCourse = (data) => {
	console.log(data);
	return Course.findById(data.courseId).then((result,err) => {
		console.log(result)
		if(data.payload.isAdmin === true){
		
				result.name = data.updatedCourse.name
				result.description = data.updatedCourse.description
				result.price = data.updatedCourse.price
	
			return result.save().then((updatedCourse, err) => {
				if(err){
					return false
				} else {
					return updatedCourse
				}
			})
		} else {
			return false
		}
	})
}

// ACTIVITY ARCHIVE COURSE

module.exports.archivedCourse = (data) => {
	
	return Course.findById(data.courseId).then((result,err) => {
		if (data.payload.isAdmin === true) {

			 result.isActive = false
			 return result.save().then((archivedCourse,err) => {
			 	if (err) {
			 		return err
			 	} else {
			 		return archivedCourse
			 	}
			 })

		} else {
			return false
		}
	})
}
