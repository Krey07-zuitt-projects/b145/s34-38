const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require('../auth');

// Checking email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Login
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


// Activity 2
// router.get('/details/:id', (req,res) => {
// 	userController.getProfile(req.params.id).then(resultFromController => res.send(resultFromController))
// });


// DISCUSSION CODE

router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId : req.body.userId, 
		courseId : req.body.courseId,
		payload : auth.decode(req.headers.authorization)
	}
	userController.enroll(data).then(resultFromController => res.send(resultFromController))
})



module.exports = router;