const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

dotenv.config();
const secret = process.env.CONNECTION_STRING;

const app = express();
const port = process.env.PORT || 4000;

mongoose.connect(secret,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("You have successfully connected to your database"));


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

app.get('/', (res, req) => {
	res.send('Hosted in Heroku')
})

app.listen(port, () => console.log(`My server is successfully running at port ${port}`))